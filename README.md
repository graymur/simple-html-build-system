### HTML build system with hot reloading

1. `yarn install` to install dependencies and build dll libraries
2. `yarn devmon` to start dev server
3. `yarn build` to build HTML/CSS/JS files

### Folder structure

`_html` - compiled HTML/CSS/JS files
`src/build` - build scripts
`src/components` - JS/CSS files of the project
`src/components/index.js` - JS entry point
`src/fonts` - fonts go here
`src/img` - images go here
`src/img` - images go here
`src/js/context.js` - put shared JS here
`src/style` - put shared styles here
`templates` - ejs templates
`templates/partials` - put shares HTML here (header, footer, etc.)
`src/styles.js` - require css from outer packages here (like bootstrap)
`src/vendors.js` - require JS from outer packages here (like jquery, jquery plugins)

